# Contenu de fichier de travail 

## Objectif de ce dépot

Ce dépot contient les fichiers dont on a besoin pour faire les activités pratiques du MOOC [Apprendre à enseigner le Numérique et les Sciences Informatiques](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/index.html).

C'est un dépot en "lecture seule" que chaque personne clone pour en avoir sa propre version et y travailler à loisir. On invite les personnes à créer des versions publiques de ce dépot pour pouvoir:

- Partager au sein du MOOC les liens des productions lors de l'activité d'évaluation par les pairs.
- Disposer d'un outil pour partager voir co-construire des ressources au delà du suivi de ce MOOC.

En effet, grâce à l'utilisation de cette plateforme "git" au delà de l'usage volontairement simplifié que nous en faisons ici, nous avons tous les mécanismes pour co-construire ensemble ces ressources pédagogiques.

## Contenu de ce dépot 

Très simplement ce dépot contient les modèles de fiches au format [markdown](https://fr.wikipedia.org/wiki/Markdown) qui sont à éditer.

- _Bien-commencer_, où il y a une activité à réaliser

Pour convenance, il contient aussi quelques copies de fichier d'information.
