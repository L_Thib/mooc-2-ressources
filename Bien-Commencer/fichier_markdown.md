# Partie 1

## Sous-partie 1 : texte

Une phrase sans rien

_Une phrase en italique_

**Une phrase en gras**

Un lien vers [fun-mooc.fr](fun-mooc.fr)

Une ligne de `code`

Voulez-vous [voir quelque chose d'amusant][un endroit amusant] ?

Eh bien, est-ce que j'ai [le site Web pour vous][un autre endroit amusant] !

[un endroit amusant]: www.zombo.com 
[un autre endroit amusant]: www.stumbleupon.com

![](https://octodex.github.com/images/bannekat.png


Nous imaginions les créatures douces et  
Douces où ils habitaient dans leur stylo de paille,  
L'un d'entre nous ne s'est pas non plus rendu compte  
Qu'un seul d'entre nous là-bas  
De douter qu'ils étaient à genoux à l'époque.


- https://daringfireball.net/projects/markdown/

- https://spec.commonmark.org/dingus/

- https://johnmacfarlane.net/babelmark2/faq.html

- https://www.markdownguide.org

- https://dave.autonoma.ca/blog/2019/05/22/typesetting-markdown-part-1/

- http://idratherbewriting.com/2013/06/04/exploring-markdown-in-collaborative-authoring-to-publishing-workflows/

- https://en.wikipedia.org/wiki/Markdown#Example

- https://docs.gitlab.com/ee/user/markdown.html

- https://docs.github.com/en/github/writing-on-github/basic-writing-and-formatting-syntax




## Sous-partie 2 : listes

### Liste à puce

- item
    - sous-item
    - sous-item
- item
- item

### Liste numérotée

1. item
2. item
2. item

## Sous-partie 3 : code

```python
    # Un extrait de code
    print('ok')
